﻿About
--
DDGUI is bunch of helper classes which puprose is to simplify most of the gui creation, to one/few lines of code.

This library will make another layer of abstraction, so I if you don`t like it or want to make more custom stuff I recommend only looking how I do things and write it yourself.

###Right now it is WIP state and it will be in this state for long time. Mistakes, bugs are sure to be encountered. Also expect that new update can break your code, as I can change basic things - I will state that very clearly!###

I will try doing frequent updates, but it depends on what I am actually working for my server mod. If there is no GUI at work, there is no updates for this library.


###If you find bugs, typos, new features I could add, have feedback or just have questions how to do something let me know! Use steam thread linked below, or write directly via bitbucket/steam/other way.###

Current features
--
* Creation of simple window with paper background, with closing button on right-top corned and title at top, that have childen layed vertically.
* * When content don`t fit the screen, vertical scrollbar appear this handles even dynamic content creation!
* * Can be positioned at center, left or right side of screen
* * As content to the window can be added:
* * * Label
* * * Button
* * * Divider
* * * TextInput
* Creation of simple panel with almost same functionality as window, but with intention being visible all the time, while allowing to have more that one on screen.

Goal
--
* Functionality wise - when my library will allow to make *all* simple things by just few lines of code
* WIP state wise - when I will cover all of base gui widgets/classes and I get sure that I am using all of that properly.
* feature wise - I want to make this library small and simple, so I cautious with adding new stuff, but rule of thumb is: If I am using specific gui code in my mods frequently that means it qualifies to this library

Gifs
--
**Support for runtime gui additions, from Example Class**

![dynamic_adding.gif](https://dl.dropboxusercontent.com/u/9278433/dagon-den/static/ddgui/dynamic_adding.gif)

Screenshots
--
**Panel next to main menu, from my Dagon Den Server Mod**
![1](http://cloud-4.steamusercontent.com/ugc/537391966976340899/2A730C2DF7C81DBEA11DDAC30511825AA1DE65AD/)

**Portion of admin tools, from my Dagon Den Server Mod**
![2](http://cloud-4.steamusercontent.com/ugc/537391966976339844/9A8997F4D7809321EA4C60298C92A90A65FEA47E/)

**Faction switching functionality from Dagon Den Server Mod:**
![3](http://cloud-4.steamusercontent.com/ugc/537391966976329161/3B4A6D7C4EEDA0A364B4683CE61B50178FFC7F6C/)

Examples
--

#####Simplest usage:#####

```
#!c#
using UnityEngine;
using Tasharen;
using DDGUI;

public class FirstMod : MonoBehaviour
{
        public void Start()
        {
            //create basic window creator that will handle creation of our "hello world" basic window
            DDGUIBasicWindowCreator windowCreator = new DDGUIBasicWindowCreator();
            //create window itself with some sample text, while providing gui object that will be parent for our window
            //yup. our window is panel, not confusing at all :)
            UIPanel window = windowCreator.CreateBasicWindow(DDGUIManager.GetMainMenuGUIParentForNewElements(), 
                "DDGUI is working!","To close this window click X button located in the right-top corner of this window");
            //show created window
            UIWindow.Show(window);
        }
}
```

Example class: [ExampleClass.cs](https://bitbucket.org/dagondev/ddgui/src/master/DDGUIExample.cs)
---

You can run example class in the Windward itself. Just grab newest builded version linked below and move folder from the zip to the "Documents/Windward/Mods"
so it should be like:

```
C:\Users\[user]\Documents\Windward\Mods\DDGUIExample\DDGUIExample.dll
C:\Users\[user]\Documents\Windward\Mods\DDGUIExample\Version.txt
C:\Users\[user]\Documents\Windward\Mods\DDGUIExample\Localization.txt
```

Builded version 
--

Newest build is always here: [DDGUIExample.zip](https://dl.dropboxusercontent.com/u/9278433/dagon-den/static/ddgui/DDGUIExample.zip)

If you doesn`t want to use it as example, then you are interested in dll only.


Why I did this?
--
I personally hate doing GUI from code and I am doing what I can to avoid it. 
But, in Windward it is necessity to write user interface programically, I am doing my best to write ugly stuff once - properly - and never look it again.

That is why I gathered all my helper classes into one library and share it with you, so you don`t need to bang your head against wall or against ArenMook in this case...