﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DDGUI
{
    /// <summary>
    /// This class gathers most important information about specific type of gui object
    /// </summary>
    public class DDGUIObjectTypeInfo
    {
        public enum Type
        {
            MAIN_MENU_ROOT,
            GAME_ROOT,
            BASIC_PAPER_WINDOW,
            BASIC_PAPER_WINDOW_BACKGROUND,
            LABEL,
            BIG_BUTTON,
            X_BUTTON,
            INPUT_WINDOW,
            SMALL_DIVIDER,
            SMALL_HEADER,
            VERTICAL_SCROLLBAR,
            CHECKBOX,
            
        }

        private int counter;
        private Type type;
        private int depth;
        private string name;

        public int Counter { get { return counter; } }
        public Type ID { get { return type; } }
        public int Depth { get { return depth; } }
        public string Name { get { return name; } }

        private DDGUIObjectTypeInfo(Type objType, int depth, string name)
        {
            this.type = objType;
            this.counter = 0;
            this.depth = depth;
            this.name = name;

        }

        internal static DDGUIObjectTypeInfo InitializeForType(Type type)
        {
            switch (type)
            {
                case Type.MAIN_MENU_ROOT:
                    return new DDGUIObjectTypeInfo(type, 1, "UI Root");
                case Type.GAME_ROOT:
                    return new DDGUIObjectTypeInfo(type, 1, "");
                case Type.BASIC_PAPER_WINDOW:
                    return new DDGUIObjectTypeInfo(type, 2, "Basic Paper Window");
                case Type.BASIC_PAPER_WINDOW_BACKGROUND:
                    return new DDGUIObjectTypeInfo(type, 3, "Basic Paper Window Background");
                case Type.LABEL:
                    return new DDGUIObjectTypeInfo(type, 5, "Label");
                case Type.BIG_BUTTON:
                    return new DDGUIObjectTypeInfo(type, 5, "Big Button");
                case Type.X_BUTTON:
                    return new DDGUIObjectTypeInfo(type, 6, "X Button");
                case Type.INPUT_WINDOW:
                    return new DDGUIObjectTypeInfo(type, 5, "Input Window");
                case Type.SMALL_DIVIDER:
                    return new DDGUIObjectTypeInfo(type, 5, "Small Divider");
                case Type.SMALL_HEADER:
                    return new DDGUIObjectTypeInfo(type, 5, "Small Header");
                case Type.VERTICAL_SCROLLBAR:
                    return new DDGUIObjectTypeInfo(type, 5, "Vertical Scrollbar");
                case Type.CHECKBOX:
                    return new DDGUIObjectTypeInfo(type, 5, "Checkbox");
                default:
                    return null;
            }
        }

        public void incrementCounter()
        {
            counter++;
        }

        public string GetDepthAsString()
        {
            return "[" + (depth < 10 ? "0" + depth : "" + depth) + "]";
        }
        public string GetNameOfSpecificGUIObject(int number)
        {
            switch (type)
            { 
                case Type.MAIN_MENU_ROOT:
                    return name;
                case Type.GAME_ROOT:
                    return GetDepthAsString();
                default:
                    return GetDepthAsString() + " " + name + " - " + number;
            }   
   
        }
        public string GetNameOfNewestGUIObject()
        {
            return GetNameOfSpecificGUIObject(counter);
        }


    }

}
