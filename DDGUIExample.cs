﻿using UnityEngine;
using DDGUI;


    /// <summary>
    /// Test example class that is showing usage of main features of this libary
    /// To use this, make folder by name of this class and move compiled dll (which rename to name of this class) to created folder, so path should be: "Documents/Windward/Mods/DDGUIExample/DDGUIExample.dll"
    /// </summary>
    public class DDGUIExample : MonoBehaviour
    {
        /// <summary>
        /// When you are in loading screen, you are de facto in world creation channel, and this is this value
        /// </summary>
        public const int WORLD_CREATION_CHANNEL = 10000;
        /// <summary>
        /// Flag that determines if client joined already
        /// </summary>
        public static bool clientJoinedAlready;
        private static int labelCounter = 0;
        /// <summary>
        /// This is going to be invoked at the start of the game
        /// </summary>
        public void Start()
        {
            //create basic window creator that will handle creation of our "hello world" basic window
            DDGUIAdvancedWidgetsCreator windowCreator = new DDGUIAdvancedWidgetsCreator();
            //create window itself with some sample text, while providing gui object that will be parent for our window
            //yup. our window is panel, not confusing at all :)
            UIPanel window = windowCreator.CreateBasicWindow(DDGUIHelper.GetMainMenuGUIParentForNewElements(), 
                "DDGUI is working!","Builded at "+Utils.RetrieveLinkerTimestamp()+".\nTo close this window click X button located in the top-right corner of this window");
            //show created window
            UIWindow.Show(window);
        }
        /// <summary>
        /// This is going to be invoked when client connects to the server
        /// </summary>
        /// <param name="success">is he connected</param>
        /// <param name="msg">if not, message about it</param>
        public virtual void OnNetworkConnect(bool success, string msg)
        {
        }
        /// <summary>
        /// This is going to be invoked when client joins any channel. that means loading screen or entering any region
        /// </summary>
        /// <param name="success"></param>
        /// <param name="errMsg"></param>
        public virtual void OnNetworkJoinChannel(bool success, string errMsg)
        {            
            if (!success)
                return;
            int channelID = TNManager.channelID; //get at what channel joining client is, if it is joining any region, channelID will be region number
            if (channelID == WORLD_CREATION_CHANNEL) //When you are in loading screen, you are de facto in world creation channel, so we don`t want to know about it
                return;
            if (!clientJoinedAlready) //we should wait with showing stuff till loading is over, but we can load some stuff while waiting
            {
                DDGUIAdvancedWidgetsCreator windowCreator = new DDGUIAdvancedWidgetsCreator(); //always create new window creator            
                UIPanel window = windowCreator.CreateBasicWindow(DDGUIHelper.GetGameGUIParentForNewElements(), "New window, yay!"); //create window only with title   
                //lets add button that will mimic closing window, just like x button
                windowCreator.AddBigButton(new EventDelegate(() => //in this way we provide delegate some functionality from somewhere else and we even can pass arguments, like window itself, cool stuff 
                {                 
                    HandleAdditionalClosingOfWindow(window);
                }), "Don`t give a damn! Let me play!"); //because we didn`t specify descText argument in CreatBasicWindow method, button will be after title               
                //we should also check how adding stuff in runtime behaves, lets add button that will add new label to window - when clicked
                windowCreator.AddBigButton(new EventDelegate(() =>
                    {
                        windowCreator.AddLabel(string.Format("This is label [000000]{0}[-]",labelCounter)); 
                        labelCounter++; //static variable that counts how many runtine labels is in the window
                    }), "Add another runtime label");
            }
            StartCoroutine(InitializeAfterLoadingScreenVanishes()); //now we have window, we should show it to the player, but only if loading is over. to make sure of that we will pack our initialize logic into coroutine that is going to be looped (while not cloging whole game by that) unit loading is no more...

        }

        /// <summary>
        /// Rountine that waits first till game loads and then initialize stuff, but only if client just joined the game
        /// </summary>
        /// <returns></returns>
        public static System.Collections.IEnumerator InitializeAfterLoadingScreenVanishes()
        {
            //while game is loading we just wait
            while (UILoadingScreen.isVisible)
            {
                yield return null;
            }
            //ok, game loaded
            if (!clientJoinedAlready)
            {
                clientJoinedAlready = true;
            }
        }
        /// <summary>
        /// Handle closing provided window
        /// </summary>
        /// <param name="window"></param>
        public void HandleAdditionalClosingOfWindow(UIPanel window)
        {
            UIWindow.Close(window);
        }

    }
