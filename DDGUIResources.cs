﻿using UnityEngine;

namespace DDGUI
{
    /// <summary>
    /// Helper static class that allow painless retreving game resources, premade ui elements. 
    /// </summary>
    public static class DDGUIResources
    {
        private static UIAtlas flagsAtlas;
        private static UIAtlas windwardAtlas;
        private static UIFont tinyFont;
        private static UIFont smallFont;
        private static UIFont mediumFont;
        private static UIFont largeFont;
        private static UIScrollBar verticalScrollBarPrefab;
        private static GameObject paperBackgroundPrefab;
        private static GameObject hudButtonPrefab;
        private static GameObject bigButtonPrefab;
        private static GameObject smallButtonPrefab;
        private static GameObject inputPrefab;
        private static GameObject smallHeaderPrefab;
        private static GameObject smallDividerPrefab;
        private static GameObject checkBoxPrefab;

        /// <summary>
        /// Not sure where it is used, but name is self explanatory
        /// </summary>
        /// <returns></returns>
        public static GameObject GetNumericInputOnlyFieldPrefab()
        {
            if (inputPrefab == null)
                inputPrefab = Resources.Load("UI/Input Field", typeof(GameObject)) as GameObject;
            return inputPrefab;
        }
        /// <summary>
        /// Big button that is used i.a. as main menu buttons
        /// </summary>
        /// <returns></returns>
        public static GameObject GetBigButtonPrefab()
        {
            if (bigButtonPrefab == null)
                bigButtonPrefab = Resources.Load("UI/Big Button", typeof(GameObject)) as GameObject;
            return bigButtonPrefab;
        }
        /// <summary>
        /// Small button that is used i.a. in ??
        /// </summary>
        /// <returns></returns>
        public static GameObject GetSmallButtonPrefab()
        {
            if (smallButtonPrefab == null)
                smallButtonPrefab = Resources.Load("UI/Small Button", typeof(GameObject)) as GameObject;
            return smallButtonPrefab;
        }
        public static GameObject GetSmallDivider()
        {
            if (smallDividerPrefab == null)
                smallDividerPrefab = Resources.Load("UI/Small Divider", typeof(GameObject)) as GameObject;
            return smallDividerPrefab;        
        }
        public static GameObject GetSmallHeader()
        {
            if (smallHeaderPrefab == null)
                smallHeaderPrefab = Resources.Load("UI/Small Header", typeof(GameObject)) as GameObject;
            return smallHeaderPrefab;
        }
        public static GameObject GetCheckbox()
        {
            if (checkBoxPrefab == null)
                checkBoxPrefab = Resources.Load("UI/Checkbox", typeof(GameObject)) as GameObject;
            return checkBoxPrefab;
        }
        /// <summary>
        /// Returns "Paper Background" background prefab that is used i.a. for main menu
        /// </summary>
        /// <returns></returns>
        public static GameObject GetPaperBackgroundPrefab()
        {
            if (paperBackgroundPrefab == null)
                paperBackgroundPrefab = Resources.Load("UI/Paper Background", typeof(GameObject)) as GameObject;
            return paperBackgroundPrefab;
        }
        /// <summary>
        /// NOT WORKING RIGHT NOW 
        /// </summary>
        /// <returns></returns>
        //public static UIScrollBar GetVerticalScrollBar()
        //{
        //    if (verticalScrollBarPrefab==null)
        //        verticalScrollBarPrefab = Resources.Load("Vertical Scrollbar", typeof(UIScrollBar)) as UIScrollBar;
        //    return verticalScrollBarPrefab;
        //}
        public static GameObject GetHUDButtonPrefab()
        {
            if (hudButtonPrefab == null)
                hudButtonPrefab = Resources.Load("UI/HUD Button", typeof(GameObject)) as GameObject;
            return hudButtonPrefab;
        }
        /// <summary>
        /// Returns Windward atlas "Atlas - Windward" that contains all of 2d/gui resources seen in game
        /// </summary>
        /// <returns></returns>
        public static UIAtlas GetWindwardUIAtlas()
        {
            if (windwardAtlas == null)
                windwardAtlas = Resources.Load("UI/Atlas - Windward", typeof(UIAtlas)) as UIAtlas;
            return windwardAtlas;
        }
        /// <summary>
        /// Returns flags atlas "Atlas - Flags" contains all the flags that appears on the left of player name
        /// </summary>
        /// <returns></returns>
        public static UIAtlas GetFlagsUIAtlas()
        {
            if (flagsAtlas == null)
                flagsAtlas = Resources.Load("UI/Atlas - Flags", typeof(UIAtlas)) as UIAtlas;
            return flagsAtlas;
        }
        /// <summary>
        /// Returns "Font - Tiny" used only for town names on the map
        /// </summary>
        /// <returns></returns>
        public static UIFont GetTinyUIFont()
        {
            if (tinyFont == null)
                tinyFont = Resources.Load("UI/Font - Tiny", typeof(UIFont)) as UIFont;
            return tinyFont;
        }
        /// <summary>
        /// Returns "Font - Small", size 18, smaller, finer font)
        /// </summary>
        /// <returns></returns>
        public static UIFont GetSmallUIFont()
        {
            if (smallFont == null)
                smallFont = Resources.Load("UI/Font - Small", typeof(UIFont)) as UIFont;
            return smallFont;
        }
        /// <summary>
        /// Returns "Font - Qlassik22"
        /// </summary>
        /// <returns></returns>
        public static UIFont GetMediumUIFont()
        {
            if (mediumFont == null)
                mediumFont = Resources.Load("UI/Font - Qlassik22", typeof(UIFont)) as UIFont;
            return mediumFont;
        }
        /// <summary>
        /// Returns "Font - Fontin", large one used for titles, size 30
        /// </summary>
        /// <returns></returns>
        public static UIFont GetLargeUIFont()
        {
            if (largeFont == null)
                largeFont = Resources.Load("UI/Font - Fontin", typeof(UIFont)) as UIFont;
            return largeFont;
        }
    }

}