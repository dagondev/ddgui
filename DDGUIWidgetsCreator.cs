﻿using UnityEngine;

namespace DDGUI
{
    /// <summary>
    /// Helper static class, simplify creation of most used gui widgets
    /// </summary>
    public static class DDGUIWidgetsCreator
    {
        public static UIToggle CreateCheckbox(GameObject parent, EventDelegate onChange, string textID, bool defaultValue=false)
        {
            GameObject prefab = DDGUIResources.GetCheckbox();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UIToggle toggle = prefabChild.GetComponent<UIToggle>();
            toggle.GetComponentInChildren<UISprite>().depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.CHECKBOX);
            toggle.GetComponentInChildren<UILabel>().text = textID;
            toggle.onChange.Add(onChange);
            toggle.value = defaultValue;
            DDGUIHelper.RegisterNewGUIObject(toggle.gameObject, DDGUIObjectTypeInfo.Type.CHECKBOX);
            return toggle;
        }
        //zakladam ze scrollbar zawsze bedzie przyklejony anchorem
        public static UIScrollBar CreateVerticalScrollBar(GameObject parent)
        {
            GameObject prefab = Resources.Load("UI/Vertical Scrollbar", typeof(GameObject)) as GameObject;
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UIScrollBar scrollBar = prefabChild.GetComponentInChildren<UIScrollBar>();
            scrollBar.backgroundWidget.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.VERTICAL_SCROLLBAR);
            DDGUIHelper.RegisterNewGUIObject(scrollBar.gameObject, DDGUIObjectTypeInfo.Type.VERTICAL_SCROLLBAR);
            return scrollBar;
        }
        public static UISprite CreateSmallDivider(GameObject parent, int width, int height)
        {
            GameObject prefab = DDGUIResources.GetSmallDivider();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UISprite sprite = prefabChild.GetComponentInChildren<UISprite>();
            sprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.SMALL_DIVIDER);
            sprite.SetDimensions(width, height);
            //no need for labels as we have same functionality in small header
            foreach(UILabel label in prefabChild.GetComponentsInChildren<UILabel>())
                NGUITools.Destroy(label);
            foreach (UILabel label in prefabChild.GetComponents<UILabel>())
                NGUITools.Destroy(label);
            DDGUIHelper.RegisterNewGUIObject(sprite.gameObject, DDGUIObjectTypeInfo.Type.SMALL_DIVIDER);
            return sprite;
            
        }
        public static UILabel CreateSmallHeader(GameObject parent, int width, int height, string headerText)
        {
            GameObject prefab = DDGUIResources.GetSmallHeader();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UISprite sprite = prefabChild.GetComponentInChildren<UISprite>();
            sprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.SMALL_HEADER);
            sprite.SetDimensions(width, height);
            UILabel label = sprite.GetComponent<UILabel>();
            label.text = headerText;
            DDGUIHelper.RegisterNewGUIObject(sprite.gameObject, DDGUIObjectTypeInfo.Type.SMALL_HEADER);
            return label;
        }
        public static UIInput CreateNumericOnlyInput(GameObject parent, int width, int height, bool getBiggerWithText = false, int maxLineCount = 1, int characterLimit = 10, bool multiLine = false)
        {
            GameObject prefab = DDGUIResources.GetNumericInputOnlyFieldPrefab();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UIInput uiInput = prefabChild.GetComponent<UIInput>();
            UISprite sprite = uiInput.GetComponentInChildren<UISprite>();
            sprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.INPUT_WINDOW);
            if (getBiggerWithText)
            {
                uiInput.label.overflowMethod = UILabel.Overflow.ResizeHeight;
                sprite.SetAnchor(uiInput.label.gameObject);
                sprite.gameObject.BroadcastMessage("UpdateAnchors");
            }
            else
            {
                uiInput.label.overflowMethod = UILabel.Overflow.ClampContent;
                sprite.SetDimensions(width, height);
            }
            uiInput.validation = UIInput.Validation.Integer;
            uiInput.label.maxLineCount = maxLineCount;
            uiInput.characterLimit = characterLimit;
            DDGUIHelper.RegisterNewGUIObject(uiInput.gameObject, DDGUIObjectTypeInfo.Type.INPUT_WINDOW);
            return uiInput;
        }
        //public static UIInput CreateInput2(GameObject parent, int width, int height, bool getBiggerWithText = false, int maxLineCount=100, int characterLimit=5000, string startingText = "", bool multiLine=true)
        //{
        //    UIInput uiInput = CreateNumericOnlyInput(parent, width, height, getBiggerWithText, maxLineCount, characterLimit, multiLine);
        //    uiInput.validation = UIInput.Validation.None;
        //    uiInput.defaultText = startingText;
        //    return uiInput;
        //}
        public static UIInput CreateInput(GameObject parent, int width, int height, bool getBiggerWithText = false, int maxLineCount=100, int characterLimit=5000, string startingText = "")
        {
            UISprite mButtonSprite = NGUITools.AddSprite(parent, DDGUIResources.GetWindwardUIAtlas(), "Chat Background");
            mButtonSprite.color = Color.black;
            mButtonSprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.INPUT_WINDOW);
            NGUITools.AddWidgetCollider(mButtonSprite.gameObject);
            UIInput uiInput = mButtonSprite.gameObject.AddComponent<UIInput>();
            uiInput.label = CreateLabel(uiInput.gameObject, width, height, DDGUIResources.GetMediumUIFont(), 20, "type here Captain!");
            uiInput.label.color = Color.white;
            uiInput.inputType = UIInput.InputType.Standard;
            uiInput.keyboardType = UIInput.KeyboardType.Default;
            uiInput.activeTextColor = Color.white;
            uiInput.characterLimit = characterLimit;
            uiInput.defaultText = startingText;
            if (maxLineCount == 1)
                uiInput.label.multiLine = false;
            else
                uiInput.label.multiLine = true;
            uiInput.label.maxLineCount = maxLineCount;

            
            if (getBiggerWithText)
            {
                uiInput.label.overflowMethod = UILabel.Overflow.ResizeHeight;
                mButtonSprite.SetAnchor(uiInput.label.gameObject);
            }
            else
            {
                mButtonSprite.SetDimensions(width, height);
                uiInput.label.SetAnchor(mButtonSprite.gameObject);
                uiInput.label.overflowMethod = UILabel.Overflow.ClampContent;
            }
            DDGUIHelper.RegisterNewGUIObject(uiInput.gameObject, DDGUIObjectTypeInfo.Type.INPUT_WINDOW);
            return uiInput;
        }
        public static UIButton CreateBigButton(GameObject parent, int width, int height, EventDelegate onClick, string buttonText)
        {
            GameObject prefab = DDGUIResources.GetBigButtonPrefab();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            prefabChild.GetComponentInChildren<UILabel>().text = buttonText;
            UISprite sprite = prefabChild.GetComponent<UISprite>();
            sprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.BIG_BUTTON);
            sprite.SetDimensions(width, height);
            UIButton button = prefabChild.GetComponent<UIButton>();
            button.onClick.Add(onClick);
            DDGUIHelper.RegisterNewGUIObject(button.gameObject, DDGUIObjectTypeInfo.Type.BIG_BUTTON);
            return button;
        }
        public static UIButton CreateXButton(GameObject parent, int width, int height, int x, int y, EventDelegate eventDelegate)
        {
            UISprite mButtonSprite = NGUITools.AddSprite(parent, DDGUIResources.GetWindwardUIAtlas(), "ButtonX");
            mButtonSprite.MakePixelPerfect();
            mButtonSprite.color = Color.black;
            mButtonSprite.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.X_BUTTON);
            NGUITools.AddWidgetCollider(mButtonSprite.gameObject);
            UIButton button = mButtonSprite.gameObject.AddComponent<UIButton>();
            mButtonSprite.SetRect(x, y, width, height);
            button.onClick.Add(eventDelegate);
            DDGUIHelper.RegisterNewGUIObject(button.gameObject, DDGUIObjectTypeInfo.Type.X_BUTTON);
            return button;
        }
        public static UILabel CreateLabel(GameObject parent, int width, int height, UIFont font, int fontSize, string text)
        {
            UILabel label = NGUITools.AddWidget<UILabel>(parent);
            label.bitmapFont = font;
            label.fontSize = fontSize;
            label.text = text;
            label.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.LABEL);
            label.SetDimensions(width, height);
            DDGUIHelper.RegisterNewGUIObject(label.gameObject, DDGUIObjectTypeInfo.Type.LABEL);
            return label;
        }
        
    }

}