﻿using UnityEngine;

namespace DDGUI
{
    /// <summary>
    /// Handle creation of simple window or panel with paper background. Creator should be created every new advanced widget!!
    /// </summary>
    public class DDGUIAdvancedWidgetsCreator
    {
        protected UIPanel parentPanel;
        protected UIPanel scrollablePanel;
        protected UIWidget background;
        protected UITable table;
        protected UIAnchor anchor;
        protected UIAnchor anchor2;
        protected UIButton xButton;
        protected UILabel titleLabel;
        protected UIScrollBar verticalScrollBar;
        protected UISprite titleDivider;
        public UIButton XButton { get { return xButton; } }
        public UILabel TitleLabel { get { return titleLabel; } }
        public int ChildrenWidth = 400;
        public int ChildrenHeight = 60;
        public int TextHeight = 60;
        public int XButtonSize = 50;
        public int XButtonPixelOffset = 15;
        public int DividerSize = 1;
        public int TablePixelOffset = 70;
        public float TableScreenOffset = 0.1f;
        public UIFont TextFont = DDGUIResources.GetMediumUIFont();
        /// <summary>
        /// creation of reusable simple window with paper background, with closing (X)button positioned at top-right of window, with title at top, that have children layed vertically. Window is always positioned at center of game window by default, but it is posible to specify position
        /// </summary>
        /// <param name="parent">gameobject of your parent panel, probably you want to pass there <see cref="DDGUIHelper.GetGameGUIParentForNewElements()"/> or <see cref="DDGUIHelper.GetMainMenuGUIParentForNewElements()"/></param>
        /// <param name="titleText">title of your window showable as text on top drawn with big large font</param>
        /// <param name="descText">more text shown below titleText drawn with medium font</param>
        /// <param name="positionAtScreen">determine at which side of screen window will appear. CURRENTLY SUPPORTED: left, right, center</param>
        /// <returns>created hided window</returns>
        public UIPanel CreateBasicWindow(GameObject parent, string titleText="", string descText = "", UIAnchor.Side positionAtScreen = UIAnchor.Side.Center)
        {
            CreateBasicPanel(parent, titleText, descText, positionAtScreen, false);

            //no need to update, because x button is added to background instead table!
            xButton = DDGUIWidgetsCreator.CreateXButton(background.gameObject, XButtonSize, XButtonSize, ChildrenWidth + XButtonPixelOffset, -TablePixelOffset + XButtonPixelOffset, new EventDelegate(() => UIWindow.Hide(parentPanel)));
            //hide because it is window, not a panel
            UIWindow.Hide(parentPanel);
            return parentPanel;
        }
        /// <summary>
        /// creation of simple panel with paper background, with closing (X)button positioned at top-right of window (that destroys panel!), with title at top, that have children layed vertically. Panel is always positioned at center of game window by default, but it is posible to specify position
        /// </summary>
        /// <param name="parent">gameobject of your parent panel, probably you want to pass there <see cref="DDGUIHelper.GetGameGUIParentForNewElements()"/>or <see cref="DDGUIHelper.GetMainMenuGUIParentForNewElements()"/></param>
        /// <param name="titleText">title of your window showable as text on top drawn with big large font</param>
        /// <param name="descText">more text shown below titleText drawn with medium font</param>
        /// <param name="createClosingButton">true if player can hide (destroy) panel</param>
        /// <returns>created panel will appear</returns>
        public UIPanel CreateBasicPanel(GameObject parent, string titleText="", string descText = "", UIAnchor.Side positionAtScreen = UIAnchor.Side.Center, bool createClosingButton = true)
        {
            parentPanel = NGUITools.AddChild<UIPanel>(parent);
            parentPanel.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.BASIC_PAPER_WINDOW);
            DDGUIHelper.RegisterNewGUIObject(parentPanel.gameObject, DDGUIObjectTypeInfo.Type.BASIC_PAPER_WINDOW);

            scrollablePanel = NGUITools.AddChild<UIPanel>(parentPanel.gameObject);
            scrollablePanel.depth = parentPanel.depth +1;
            scrollablePanel.clipping = UIDrawCall.Clipping.SoftClip;
            scrollablePanel.clipSoftness = new Vector2(5, 15);
            scrollablePanel.baseClipRegion = new Vector4(0, -TablePixelOffset/5, parentPanel.GetWindowSize().x, parentPanel.GetWindowSize().y - TablePixelOffset * 3.5f);

            UIFont font = DDGUIResources.GetMediumUIFont();
            UIFont bigFont = DDGUIResources.GetLargeUIFont();

            anchor = CreateAnchor(parentPanel.gameObject,positionAtScreen);
            background = CreateBackground(anchor.gameObject);

            if (!titleText.Equals(""))
            {
                titleLabel = DDGUIWidgetsCreator.CreateLabel(background.gameObject, ChildrenWidth, 80, bigFont, 30, titleText);
                titleLabel.SetRect(TablePixelOffset * 0.5f, -TablePixelOffset - XButtonPixelOffset * 0.5f, ChildrenWidth, 80);
                titleDivider = DDGUIWidgetsCreator.CreateSmallDivider(background.gameObject, ChildrenWidth, DividerSize);
                titleDivider.SetRect(-TablePixelOffset * 0.25f, -TablePixelOffset - XButtonPixelOffset * 0.5f, ChildrenWidth * 0.5f - TablePixelOffset * 0.5f, DividerSize);
            }

            anchor2 = CreateAnchor(scrollablePanel.gameObject, positionAtScreen);
            table = CreateTable(anchor2.gameObject);

            if (!descText.Equals(""))
            {
                AddLabel(ChildrenWidth, TextHeight, TextFont, TextFont.defaultSize, descText);
            }

            Update();
            //no need to update, because x button is added to background instead to table!
            if(createClosingButton)
                //TODO: przyklejac anchorem do backgroundu/tabeli co by nie polegac na DEFAULT_CHILDREN_WIDTH!
                xButton = DDGUIWidgetsCreator.CreateXButton(background.gameObject, XButtonSize, XButtonSize, ChildrenWidth + XButtonPixelOffset, -TablePixelOffset + XButtonPixelOffset,
                new EventDelegate(() => { 
                    DestroyPanel();
                }));
            
            verticalScrollBar = DDGUIWidgetsCreator.CreateVerticalScrollBar(background.gameObject);
            verticalScrollBar.value = 0;
            verticalScrollBar.gameObject.SetActive(false);
            verticalScrollBar.onChange.Add(new EventDelegate(()=> 
            {
                Bounds bound = NGUIMath.CalculateRelativeWidgetBounds(table.transform);
                float sizeY = bound.size.y + TablePixelOffset + parentPanel.GetWindowSize().y * TableScreenOffset*0.5f;
                float relativeY = sizeY / parentPanel.GetWindowSize().y;
                float screenOffsetY=(relativeY-1)*0.5f;
                if (screenOffsetY < 0)
                    screenOffsetY *= -1;
                anchor2.relativeOffset = new Vector2(anchor2.relativeOffset.x, -TableScreenOffset - screenOffsetY + verticalScrollBar.value*screenOffsetY*2.5f );
            }));

            //must be after update
            table.transform.position = new Vector3(table.transform.position.x, table.transform.position.y - TableScreenOffset * 0.75f, table.transform.position.z);

            return parentPanel;
        }
        /// <summary>
        /// Will be called by default when user click X button in the top-left corner.
        /// After destroying all crated references are setted to null
        /// </summary>
        public void DestroyPanel()
        {
            foreach (Transform widget in table.GetChildList())
                NGUITools.Destroy(widget); 
            NGUITools.Destroy(table);
            NGUITools.Destroy(scrollablePanel);
            NGUITools.Destroy(anchor2);
            foreach (UIWidget widget in background.GetComponentsInChildren<UIWidget>())
                NGUITools.Destroy(widget);
            foreach (UIWidget widget in background.GetComponentsInChildren<UISprite>())
                NGUITools.Destroy(widget);
            NGUITools.Destroy(background);
            NGUITools.Destroy(anchor);
            foreach (UIWidget widget in parentPanel.GetComponentsInChildren<UIWidget>())
                NGUITools.Destroy(widget);
            foreach (UISprite widget in parentPanel.GetComponentsInChildren<UISprite>())
                NGUITools.Destroy(widget);
            NGUITools.Destroy(parentPanel);
            parentPanel = null;
            table = null;
            anchor = null;
            background = null;
            xButton = null;
            verticalScrollBar = null;
            titleDivider = null;

            //uiScrollView = null;
        }
        /// <summary>
        /// Invoke this if you added something to the table of window
        /// </summary>
        public void Update()
        {
            table.Reposition();
            Bounds bound = NGUIMath.CalculateRelativeWidgetBounds(table.transform);
            float sizeX = bound.size.x + TablePixelOffset;
            float sizeY = bound.size.y + TablePixelOffset * 0.5f + parentPanel.GetWindowSize().y*TableScreenOffset;
            float usuableWindowSizeY = parentPanel.GetWindowSize().y-TablePixelOffset*2;
            float halfOffset = TablePixelOffset * 0.5f;
            //if content doesn`t fit screen show scrollbar and reposition table via anchor
            if(sizeY>usuableWindowSizeY&&(verticalScrollBar != null&&verticalScrollBar.gameObject != null))
            {            
                verticalScrollBar.gameObject.SetActive(true); 
                sizeY=usuableWindowSizeY;
                foreach (EventDelegate eventDelegate in verticalScrollBar.onChange)
                    eventDelegate.Execute();
            }
            
            background.SetRect(0, -sizeY, sizeX, sizeY);
            if (titleDivider != null)
            {
                //hack 
                //TODO: instead destroying, make repositioning divider somehow still relative to background
                //or just add third anchor for title and divider...
                NGUITools.Destroy(titleDivider);
                titleDivider = DDGUIWidgetsCreator.CreateSmallDivider(background.gameObject, ChildrenWidth, DividerSize);
                titleDivider.SetRect(-TablePixelOffset * 0.25f, -TablePixelOffset - XButtonPixelOffset * 0.5f, ChildrenWidth * 0.5f - TablePixelOffset * 0.5f, DividerSize);
            }
            float relativeX = sizeX / parentPanel.GetWindowSize().x;
            float relativeY = sizeY / parentPanel.GetWindowSize().y;
            //TODO: zrobic reszte przypadkow
            switch(anchor.side)
            {
                case UIAnchor.Side.Center:
                    anchor.relativeOffset = new Vector2(-relativeX * 0.5f, relativeY * 0.5f);
                    anchor2.relativeOffset = new Vector2(anchor2.relativeOffset.x, anchor2.relativeOffset.y);
                    break;
                case UIAnchor.Side.Right:
                    anchor.relativeOffset = new Vector2(-relativeX, relativeY * 0.5f);
                    anchor2.relativeOffset = new Vector2(-relativeX/2, anchor2.relativeOffset.y);
                    break;
                case UIAnchor.Side.Left:
                    anchor.relativeOffset = new Vector2(relativeX, relativeY * 0.5f);
                    anchor2.relativeOffset = new Vector2(relativeX, anchor2.relativeOffset.y);
                    break;
            }
            
            background.gameObject.BroadcastMessage("UpdateAnchors");
            if (xButton != null)
            {               
                xButton.GetComponent<UISprite>().SetRect(ChildrenWidth*table.columns + XButtonPixelOffset, -TablePixelOffset + XButtonPixelOffset, XButtonSize, XButtonSize);
            }
            if(verticalScrollBar!=null)
                if (verticalScrollBar.backgroundWidget != null)
                    verticalScrollBar.backgroundWidget.SetRect(ChildrenWidth + XButtonPixelOffset * 2, -sizeY + TablePixelOffset/2, 20, sizeY - TablePixelOffset * 2);
        }
        public UIToggle AddCheckbox(string textID,EventDelegate onChange,bool defaultValue=false)
        {
            UIToggle toggle = DDGUIWidgetsCreator.CreateCheckbox(table.gameObject,onChange,textID,defaultValue);

            Update();
            return toggle;
        }
        public UISprite AddSmallDivider()
        {
            UISprite sprite = DDGUIWidgetsCreator.CreateSmallDivider(table.gameObject, ChildrenWidth, ChildrenHeight);
            Update();
            return sprite;
        }
        //TODO:zrobic opis
        public UIInput AddInput(int width, int height, bool getBiggerWithText=false, int lineCount=10, int characterLimit=5000, string startingText = "Start typing here, Captain!")
        {
            UIInput input = DDGUIWidgetsCreator.CreateInput(table.gameObject, width, height, getBiggerWithText, lineCount, characterLimit, startingText);
            if(getBiggerWithText)
                input.onChange.Add(new EventDelegate(() => { Update(); }));
            Update();
            return input;
        }
        public UIInput AddNumericInput() 
        {
            UIInput input = DDGUIWidgetsCreator.CreateNumericOnlyInput(table.gameObject, ChildrenWidth, ChildrenHeight);
            Update();
            return input;
        }
        /// <summary>
        /// Add label to the window with provided text, created with default values
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public UILabel AddLabel(string text)
        {
            return AddLabel(ChildrenWidth, TextHeight, TextFont, TextFont.defaultSize, text);
        }
        /// <summary>
        /// Add label to the window with provided text, while width, height
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public UILabel AddLabel(int width, int height, string text)
        {
            UILabel label = DDGUIWidgetsCreator.CreateLabel(table.gameObject, width, height, TextFont, TextFont.defaultSize, text);
            Update();
            return label;
        }
        /// <summary>
        /// Add label to the window with provided text, while specifing font, fontsize, width, height
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="font"></param>
        /// <param name="fontSize"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public UILabel AddLabel(int width, int height, UIFont font, int fontSize, string text)
        {
            UILabel label = DDGUIWidgetsCreator.CreateLabel(table.gameObject, width, height, font, fontSize, text);
            Update();
            return label;
        }
        /// <summary>
        /// Add big button to the window with provided text and delegate that handles onClick event
        /// </summary>
        /// <param name="onClickDelegate"></param>
        /// <param name="buttonText"></param>
        /// <returns></returns>
        public UIButton AddBigButton(EventDelegate onClickDelegate, string buttonText)
        {
            return AddBigButton(ChildrenWidth, ChildrenHeight, onClickDelegate, buttonText);
        }
        /// <summary>
        ///  Add big button to the window with provided text and delegate that handles onClick event, while specificing his width and height
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="onClickDelegate"></param>
        /// <param name="buttonText"></param>
        /// <returns></returns>
        public UIButton AddBigButton(int width, int height, EventDelegate onClickDelegate, string buttonText)
        {
            UIButton button = DDGUIWidgetsCreator.CreateBigButton(table.gameObject, width, height, onClickDelegate, buttonText);
            Update();
            return button;
        }
        /// <summary>
        /// Add "X" icon button with specific coordinates (relative to background) and delegate that handles onClick event
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="onClickDelegate"></param>
        /// <returns></returns>
        public UIButton AddXButton(int x, int y, EventDelegate onClickDelegate)
        {
            return AddXButton(XButtonSize, XButtonSize, x, y, onClickDelegate);
        }
        /// <summary>
        ///  Add "X" icon button with specific coordinates (relative to background) and delegate that handles onClick eventm while specifing his width and height
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="eventDelegate"></param>
        /// <returns></returns>
        public UIButton AddXButton(int width, int height, int x, int y, EventDelegate eventDelegate)
        {
            UIButton button = DDGUIWidgetsCreator.CreateXButton(table.gameObject, width, height, x, y, eventDelegate);
            Update();
            return button;
        }
        protected UIAnchor CreateAnchor(GameObject parent,UIAnchor.Side positionAtScreen)
        {
            UIAnchor anchor = NGUITools.AddChild<UIAnchor>(parent);
            anchor.side = positionAtScreen;
            anchor.runOnlyOnce = false;
            return anchor;
        }
        /// <summary>
        /// Create paper background for provided parent
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected UIWidget CreateBackground(GameObject parent)
        {
            GameObject prefab = DDGUIResources.GetPaperBackgroundPrefab();
            GameObject prefabChild = NGUITools.AddChild(parent, prefab);
            UIWidget bckg = prefabChild.GetComponent<UIWidget>();
            bckg.pivot = UIWidget.Pivot.TopLeft;
            bckg.depth = DDGUIHelper.GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type.BASIC_PAPER_WINDOW_BACKGROUND);
            DDGUIHelper.RegisterNewGUIObject(bckg.gameObject, DDGUIObjectTypeInfo.Type.BASIC_PAPER_WINDOW_BACKGROUND);

            return bckg;
        }
        /// <summary>
        /// create table with provided background as parent
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected UITable CreateTable(GameObject parent)
        {
            UITable tbl = NGUITools.AddChild<UITable>(parent);
            tbl.columns = 1;
            //tbl.cellAlignment = UIWidget.Pivot.TopLeft;
            tbl.pivot = UIWidget.Pivot.Center;
            tbl.sorting = UITable.Sorting.None;
            tbl.direction = UITable.Direction.Down;
            tbl.hideInactive = true;
            
            return tbl;
        }
    }
}