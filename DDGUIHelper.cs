﻿using UnityEngine;

namespace DDGUI
{
    /// <summary>
    /// Helper static class that manages gui objects created via DDGUI and contains useful methods that are somehow related to managing GUI, creation, editing etc. and yet doesn`t get to other classes
    /// </summary>
    public static class DDGUIHelper
    {
        private static System.Collections.Generic.Dictionary<DDGUIObjectTypeInfo.Type, DDGUIObjectTypeInfo> guiObjectTypesInfos;
        private const string CHAT_MSG_COLOR_END_TAG = "[-]";
        static DDGUIHelper()
        {
            guiObjectTypesInfos = new System.Collections.Generic.Dictionary<DDGUIObjectTypeInfo.Type, DDGUIObjectTypeInfo>();
            foreach (DDGUIObjectTypeInfo.Type type in DDGUIObjectTypeInfo.Type.GetValues(typeof(DDGUIObjectTypeInfo.Type)))
                guiObjectTypesInfos.Add(type, DDGUIObjectTypeInfo.InitializeForType(type));
        }
        /// <summary>
        /// Sets name for gameObject, written in convention that is used in naming ui elements: "[depth] typeOfObject - number", increment counter of that type of guiObject by one and returns name
        /// </summary>
        /// <param name="depth"></param>
        /// <returns></returns>
        public static string RegisterNewGUIObject(GameObject gameObject, DDGUIObjectTypeInfo.Type typeOfObject)
        {
            if (!guiObjectTypesInfos.ContainsKey(typeOfObject))
                guiObjectTypesInfos.Add(typeOfObject, DDGUIObjectTypeInfo.InitializeForType(typeOfObject));
            else
                guiObjectTypesInfos[typeOfObject].incrementCounter();
            string name = GetNewestGUIObjectName(typeOfObject);
            gameObject.name = name;
            return name;
        }

        /// <summary>
        /// Retrieve gui parent element when in game
        /// </summary>
        /// <returns>UIRoot</returns>
        public static GameObject GetGameGUIParentForNewElements()
        {
            return GameObject.Find(GetNewestGUIObjectName(DDGUIObjectTypeInfo.Type.GAME_ROOT));
        }
        /// <summary>
        /// Retrieve gui parent element when in main menu
        /// </summary>
        /// <returns>UIRoot</returns>
        public static GameObject GetMainMenuGUIParentForNewElements()
        {
            return GameObject.Find(GetNewestGUIObjectName(DDGUIObjectTypeInfo.Type.MAIN_MENU_ROOT));
        }
        public static string GetNewestGUIObjectName(DDGUIObjectTypeInfo.Type guiObjectType)
        {
            return guiObjectTypesInfos[guiObjectType].GetNameOfNewestGUIObject();
        }
        public static string GetSpecificGUIObjectName(DDGUIObjectTypeInfo.Type guiObjectType,int number)
        {
            return guiObjectTypesInfos[guiObjectType].GetNameOfSpecificGUIObject(number);
        }
        public static int GetNumberOfGUIObjectsType(DDGUIObjectTypeInfo.Type guiObjectType)
        {
            return guiObjectTypesInfos[guiObjectType].Counter;
        }
        public static int GetDepthLevelGUIObjectType(DDGUIObjectTypeInfo.Type guiObjectType)
        {
            return guiObjectTypesInfos[guiObjectType].Depth;
        }
        public static string SetColorForMessageInUILabel(string msg, Color color)
        {
            return "[" + Utils.ColorToHex(color) + "]" + msg + CHAT_MSG_COLOR_END_TAG;
        }
    }
}
