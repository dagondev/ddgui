﻿using UnityEngine;
using TNet;
using System.Text;

namespace DDGUI
{
    /// <summary>
    /// Helper static debug class
    /// </summary>
    /// 
    public static class DDGUIDebug
    {
        /// <summary>
        /// Defines if methods in this class are going to log anything
        /// </summary>
        public static bool DebugOn = false;
        /// <summary>
        /// Log object at left top corner of screen and to server logs.
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        /// <param name="objs"></param>
        public static void Log(object[] objs)
        {
            if (!DebugOn)
                return;
            NGUIDebug.Log(objs);
        }
        /// <summary>
        /// Log object at left top corner of screen and to server logs.
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        /// <param name="objs"></param>
        public static void Log(object obj)
        {
            if (!DebugOn)
                return;
            NGUIDebug.Log(obj);
            TNManager.Log(obj.ToString());
        }
        /// <summary>
        /// Write info about data node and every children that it has
        /// File is <code>dataNode.name+"_DataNode_Dump.txt"</code> that is located in Windward game folder
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        public static void PrintDataNodeObjectToFile(DataNode dataNode)
        {
            if (!DebugOn)
                return;
            if (dataNode == null)
                return;
            System.IO.StreamWriter file = new System.IO.StreamWriter(dataNode.name + "_DataNode_Dump.txt");
            file.WriteLine(WriteDataNodeObjectToString(dataNode));
            file.Close();
        }
        /// <summary>
        /// Write all sprite names from all atlases to file.
        /// File is <code>uiAtlas.name+"_AtlasSpriteListDump.txt"</code> that is located in Windward game folder
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        public static void DumpAtlasesSpriteNames()
        {
            if (!DebugOn)
                return;
            UIAtlas[] atlases = new UIAtlas[] { DDGUIResources.GetWindwardUIAtlas(), DDGUIResources.GetFlagsUIAtlas() };
            foreach (UIAtlas uiAtlas in atlases)
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(uiAtlas.name + "SpriteListDump.txt");
                foreach (UISpriteData uiSpriteData in uiAtlas.spriteList)
                {
                    file.WriteLine(uiSpriteData.name);
                }
                file.Close();
            }
        }
        /// <summary>
        /// Writes what components can be found in the UIRoot to file.
        /// File is <code>stage+"_Stage_UIRootItemsListDump.txt"</code> that is located in Windward game folder
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        /// <param name="stage">name stage at what this method is invoked to distinguish file from another, i.e. "Main Menu"</param>
        public static void PrintUIRootElementsToFile(string stage)
        {
            if (!DebugOn)
                return;
            System.IO.StreamWriter file = new System.IO.StreamWriter(stage + "_Stage_UIRootItemsListDump.txt");
            foreach (Transform t in UIRoot.list[0].transform)
            {
                file.WriteLine(t.name);
            }
            file.Close();
        }
        /// <summary>
        /// Right now writes what components can be found in gameObject to file.
        /// File is <code>uiAtlas.name+"_GameObjectDump.txt"</code> that is located in Windward game folder
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        /// <param name="gameObject"></param>
        public static void PrintGameObjectElementsToFile(GameObject gameObject)
        {
            if (!DebugOn)
                return;
            System.IO.StreamWriter file = new System.IO.StreamWriter(gameObject.name + "_GameObjectDump.txt");
            file.Write(WriteGameObjectElementsToString(gameObject));
            file.Close();
        }
        /// <summary>
        /// Right now writes what children components can be found in gameObject to file.
        /// File is <code>uiAtlas.name+"_GameObjectChildrenDump.txt"</code> that is located in Windward game folder
        /// Only when <see cref="DDGUIDebug.DebugOn"/> is true
        /// </summary>
        /// <param name="gameObject"></param>
        public static void PrintGameObjectChildrenElementsToFile(GameObject gameObject)
        {
            if (!DebugOn)
                return;
            System.IO.StreamWriter file = new System.IO.StreamWriter(gameObject.name + "_GameObjectChildrenDump.txt");
            file.Write(WriteGameObjectChildrenElementsToString(gameObject));
            file.Close();
        }
        /// <summary>
        /// Right now writes what components can be found in gameObject to file.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static string WriteGameObjectElementsToString(GameObject gameObject)
        {
            if (gameObject == null)
                return "object is null";
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Name:" + gameObject.name);
            UIPanel[] panels = gameObject.GetComponents<UIPanel>();
            UIWidget[] widgets = gameObject.GetComponents<UIWidget>();
            UIWidgetContainer[] widgetContainets = gameObject.GetComponents<UIWidgetContainer>();
            UIWindow[] windows = gameObject.GetComponents<UIWindow>();
            UIWindowButton[] windowButtons = gameObject.GetComponents<UIWindowButton>();
            UIAnchor[] anchors = gameObject.GetComponents<UIAnchor>();
            UIButton[] buttons = gameObject.GetComponents<UIButton>();
            UILabel[] labels = gameObject.GetComponents<UILabel>();
            UIPopupList[] poplists = gameObject.GetComponents<UIPopupList>();
            UIProgressBar[] progressBars = gameObject.GetComponents<UIProgressBar>();
            UISkillBar[] skillBars = gameObject.GetComponents<UISkillBar>();
            UISlider[] sliders = gameObject.GetComponents<UISlider>();
            UISprite[] sprites = gameObject.GetComponents<UISprite>();
            UI2DSprite[] sprites2D = gameObject.GetComponents<UI2DSprite>();
            UIStatusBar[] statusBars = gameObject.GetComponents<UIStatusBar>();
            UITable[] tables = gameObject.GetComponents<UITable>();
            UITooltip[] tooltips = gameObject.GetComponents<UITooltip>();
            builder.AppendLine("panels:" + panels.Length);
            builder.AppendLine("widgets:" + widgets.Length);
            builder.AppendLine("widgetContainets:" + widgetContainets.Length);
            builder.AppendLine("tables:" + tables.Length);
            builder.AppendLine("anchors:" + anchors.Length);
            builder.AppendLine("sprites:" + sprites.Length);
            builder.AppendLine("sprites2d:" + sprites2D.Length);
            builder.AppendLine("buttons:" + buttons.Length);
            builder.AppendLine("labels:" + labels.Length);
            builder.AppendLine("poplists:" + poplists.Length);
            builder.AppendLine("progressBars:" + progressBars.Length);
            builder.AppendLine("skillBars:" + skillBars.Length);
            builder.AppendLine("sliders:" + sliders.Length);
            builder.AppendLine("statusBars:" + statusBars.Length);
            return builder.ToString();
        }
        /// <summary>
        /// Right now writes what components can be found in gameObject to file.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static string WriteGameObjectChildrenElementsToString(GameObject gameObject)
        {
            if (gameObject == null)
                return "object is null";
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Name:" + gameObject.name);
            UIPanel[] panels = gameObject.GetComponents<UIPanel>();
            UIWidget[] widgets = gameObject.GetComponentsInChildren<UIWidget>();
            UIWidgetContainer[] widgetContainets = gameObject.GetComponentsInChildren<UIWidgetContainer>();
            UIWindow[] windows = gameObject.GetComponentsInChildren<UIWindow>();
            UIWindowButton[] windowButtons = gameObject.GetComponentsInChildren<UIWindowButton>();
            UIAnchor[] anchors = gameObject.GetComponentsInChildren<UIAnchor>();
            UIButton[] buttons = gameObject.GetComponentsInChildren<UIButton>();
            UILabel[] labels = gameObject.GetComponentsInChildren<UILabel>();
            UIPopupList[] poplists = gameObject.GetComponentsInChildren<UIPopupList>();
            UIProgressBar[] progressBars = gameObject.GetComponentsInChildren<UIProgressBar>();
            UISkillBar[] skillBars = gameObject.GetComponentsInChildren<UISkillBar>();
            UISlider[] sliders = gameObject.GetComponentsInChildren<UISlider>();
            UISprite[] sprites = gameObject.GetComponentsInChildren<UISprite>();
            UI2DSprite[] sprites2D = gameObject.GetComponentsInChildren<UI2DSprite>();
            UIStatusBar[] statusBars = gameObject.GetComponentsInChildren<UIStatusBar>();
            UITable[] tables = gameObject.GetComponentsInChildren<UITable>();
            UITooltip[] tooltips = gameObject.GetComponentsInChildren<UITooltip>();
            builder.AppendLine("panels:" + panels.Length);
            builder.AppendLine("widgets:" + widgets.Length);
            builder.AppendLine("widgetContainets:" + widgetContainets.Length);
            builder.AppendLine("tables:" + tables.Length);
            builder.AppendLine("anchors:" + anchors.Length);
            builder.AppendLine("sprites:" + sprites.Length);
            builder.AppendLine("sprites2d:" + sprites2D.Length);
            builder.AppendLine("buttons:" + buttons.Length);
            builder.AppendLine("labels:" + labels.Length);
            builder.AppendLine("poplists:" + poplists.Length);
            builder.AppendLine("progressBars:" + progressBars.Length);
            builder.AppendLine("skillBars:" + skillBars.Length);
            builder.AppendLine("sliders:" + sliders.Length);
            builder.AppendLine("statusBars:" + statusBars.Length);
            return builder.ToString();
        }
        /// <summary>
        /// Write info about data node and every children that it has
        /// </summary>
        /// <param name="dataNode"></param>
        /// <returns></returns>
        public static string WriteDataNodeObjectToString(DataNode dataNode)
        {
            return WriteDataNodeObjectToString(dataNode, 0);
        }
        private static string WriteDataNodeObjectToString(DataNode dataNode, int level)
        {
            if (dataNode == null)
                return "object is null";
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(new string('\t', level) + "Name:" + dataNode.name);
            if (dataNode.type != null)
                builder.AppendLine(new string('\t', level) + "Type:" + dataNode.type.ToString());
            if(dataNode.value!=null)
                builder.AppendLine(new string('\t', level) + "Value:" + dataNode.value.ToString());
            if(dataNode.children==null)
                return builder.ToString();
            if (dataNode.children.Count > 0)
            {
                builder.AppendLine(new string('\t', level) + "Children: (" + dataNode.children.Count+")");
                builder.AppendLine(new string('\t', level) +"[");
                foreach (DataNode dataNodeChildren in dataNode.children)
                    builder.AppendLine(WriteDataNodeObjectToString(dataNodeChildren, level + 1));
                builder.AppendLine(new string('\t', level) + "]");
            }
            return builder.ToString();
        }
    }

}